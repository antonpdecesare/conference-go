import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)

    # remember to print so you know what to parse down (0 index, then source, then original)

    return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    geocode = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid=8e2f3f7faa7be5ca5bbb88fbcbccfb09"
    response = requests.get(geocode)

    lat = response.json()[0].get('lat')
    lon = response.json()[0].get('lon')
    weather_data = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=8e2f3f7faa7be5ca5bbb88fbcbccfb09&units=imperial"
    weather_data_response = requests.get(weather_data)

    temp = weather_data_response.json().get('main').get('temp')
    weather_description = weather_data_response.json().get('weather')[0].get("description")
    return {
        "temp": temp,
        "weather_description": weather_description
    }
